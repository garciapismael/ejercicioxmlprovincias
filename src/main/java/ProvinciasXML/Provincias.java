package ProvinciasXML;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;

@XmlRootElement(name = "provincias")
@XmlType(propOrder = "listaProvincias")
public class Provincias {
    private ArrayList<Provincia> listaProvincias;

    public Provincias() {
    }

    public Provincias(ArrayList<Provincia> listaProvincias) {
        this.listaProvincias = listaProvincias;
    }

    @XmlElement(name = "provincia")
    public ArrayList<Provincia> getListaProvincias() {
        return listaProvincias;
    }
    public void setListaProvincias(ArrayList<Provincia> listaProvincias) {
        this.listaProvincias = listaProvincias;
    }
}
