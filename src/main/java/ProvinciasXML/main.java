package ProvinciasXML;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class main {
    private static String archivo = "provincias.xml";

    public static void main(String[] args) {

        Pueblo pueblo = new Pueblo();
        pueblo.setNum(1);
        pueblo.setNombre_pu("Containa");
        pueblo.setPoblacion_pu(100);
        pueblo.setAltitud(200);

        Pueblo pueblo1 = new Pueblo();
        pueblo1.setNum(2);
        pueblo1.setNombre_pu("Alcoy");
        pueblo1.setPoblacion_pu(200);
        pueblo1.setAltitud(300);

        ArrayList<Pueblo> listapueblos = new ArrayList<>();
        listapueblos.add(pueblo);
        listapueblos.add(pueblo1);

        Provincia provincia = new Provincia();
        provincia.setNombre_pr("Alicante");
        provincia.setPoblacion_pr(3000);
        provincia.setSuperficie_pr(30000);
        provincia.setListaPueblos(listapueblos);

        Provincia provincia1 = new Provincia();
        provincia1.setNombre_pr("Valencia");
        provincia1.setPoblacion_pr(4000);
        provincia1.setSuperficie_pr(40000);
        provincia1.setListaPueblos(listapueblos);

        ArrayList<Provincia> listaProvincias = new ArrayList<>();
        listaProvincias.add(provincia);
        listaProvincias.add(provincia1);

        Provincias provincias = new Provincias();
        provincias.setListaProvincias(listaProvincias);

        try {
            JAXBContext context = JAXBContext.newInstance(Provincias.class);
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            m.marshal(provincias, System.out);

            try (FileWriter fw = new FileWriter(archivo)) {
                m.marshal(provincias, fw);
            } catch (IOException ex) {
                System.err.println("Problemas durante la escritura del fichero..." + ex.getMessage());
            }

            Unmarshaller um = context.createUnmarshaller();
            Provincias Provincias = null;
            try {
                Provincias = (Provincias) um.unmarshal(new FileReader(archivo));
            } catch (FileNotFoundException ex) {
                System.err.println("Parece que el fichero no existe..." + ex.getMessage());
            }
            if (Provincias != null) {
                listaProvincias.stream().forEach(System.out::println);
            }
        } catch (JAXBException ex) {
            System.out.println("Problemas durante la lectura/escritura del archivo xml " + archivo + " " + ex.getMessage());
        }
    }

}
