package ProvinciasXML;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {"nombre_pu","poblacion_pu","altitud"})
public class Pueblo {
    private int num;
    private String nombre_pu;
    private int poblacion_pu;
    private int altitud;

    public Pueblo() {
    }

    public Pueblo(int num, String nombre_pu, int poblacion_pu, int altitud) {
        this.num = num;
        this.nombre_pu = nombre_pu;
        this.poblacion_pu = poblacion_pu;
        this.altitud = altitud;
    }

    @XmlAttribute(name = "num")
    public int getNum() {
        return num;
    }
    public void setNum(int num) {
        this.num = num;
    }

    @XmlElement(name = "nombre_pu")
    public String getNombre_pu() {
        return nombre_pu;
    }
    public void setNombre_pu(String nombre_pu) {
        this.nombre_pu = nombre_pu;
    }

    @XmlElement(name = "poblacion_pu")
    public int getPoblacion_pu() {
        return poblacion_pu;
    }
    public void setPoblacion_pu(int poblacion_pu) {
        this.poblacion_pu = poblacion_pu;
    }

    @XmlElement(name = "altitud")
    public int getAltitud() {
        return altitud;
    }
    public void setAltitud(int altitud) {
        this.altitud = altitud;
    }
}
