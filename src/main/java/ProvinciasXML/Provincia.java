package ProvinciasXML;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;


@XmlType(propOrder = {"nombre_pr","superficie_pr","poblacion_pr", "listaPueblos"})
public class Provincia {
    private String nombre_pr;
    private int superficie_pr;
    private int poblacion_pr;
    private ArrayList<Pueblo> listaPueblos;

    public Provincia() {
    }

    public Provincia(String nombre_pr, int superficie_pr, int poblacion_pr, ArrayList<Pueblo> listaPueblos) {
        this.nombre_pr = nombre_pr;
        this.superficie_pr = superficie_pr;
        this.poblacion_pr = poblacion_pr;
        this.listaPueblos = listaPueblos;
    }

    @XmlElement(name = "nombre_pr")
    public String getNombre_pr() {
        return nombre_pr;
    }
    public void setNombre_pr(String nombre_pr) {
        this.nombre_pr = nombre_pr;
    }

    @XmlElement(name = "superficie_pr")
    public int getSuperficie_pr() {
        return superficie_pr;
    }
    public void setSuperficie_pr(int superficie_pr) {
        this.superficie_pr = superficie_pr;
    }

    @XmlElement(name = "poblacion_pr")
    public int getPoblacion_pr() {
        return poblacion_pr;
    }
    public void setPoblacion_pr(int poblacion_pr) {
        this.poblacion_pr = poblacion_pr;
    }

    @XmlElement(name = "pueblo")
    public ArrayList<Pueblo> getListaPueblos() {
        return listaPueblos;
    }
    public void setListaPueblos(ArrayList<Pueblo> listaPueblos) {
        this.listaPueblos = listaPueblos;
    }
}
